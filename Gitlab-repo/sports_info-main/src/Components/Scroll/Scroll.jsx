import React, { useState, useEffect } from 'react'

import { IconButton } from '@material-ui/core'

const Scroll = (showBelow) => {
  const [show, setShow] = useState(showBelow ? 'false' : 'true')

  const handleScroll = () => {
    if (window.pageYOffset > showBelow) {
      if (!show) setShow(true)
    } else {
      if (show) setShow(false)
    }
  }

  useEffect(() => {
    if (showBelow) {
      window.addEventListener('scroll', handleScroll)
      return () => window.removeEventListener('scroll', handleScroll)
    }
  })

  return (
        <div>
            <IconButton>

            </IconButton>
        </div>
  )
}

export default Scroll
