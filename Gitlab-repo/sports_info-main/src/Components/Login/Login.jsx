// import React from 'react'
// import { Formik, Field } from "formik";
// import { v4 as uuidv4 } from "uuid";
// import {  useHistory } from "react-router-dom";

// /****************************Material-UI-Components*********************************************/
// import Grid from '@material-ui/core/Grid';
// import Card from '@material-ui/core/Card';
// import Container from '@material-ui/core/Container';
// import Button from '@material-ui/core/Button'

// /**********************Yup-Validation****************************************/
// import * as Yup from 'yup';

// /****************************Material-UI-Icons************************************************/

// import SportsCricketTwoToneIcon from '@material-ui/icons/SportsCricketTwoTone';

// /****************************Css-File********************************************************/
// import './login.css'

// /**********************Validation-Schema****************************************/
// const ValidationSchema = Yup.object().shape({
//   uname: Yup.string()
//     .required('Username is required')
//     .matches(/^(\S)/, 'This field cannot contain only blankspaces'),
//   email: Yup.string().email()
//     .required('Email is required')
//     .matches(/^(\S)/, 'This field cannot contain only blankspaces'),
//   pass: Yup.string()
//     .required('Password is required')
//     .matches(/^(\S)/, 'This field cannot contain only blankspaces'),
// });

// function Login() {
//     const history = useHistory();
//     const onHandleSubmit = (values) => {
//         console.log("values",values);
//         if (values.uname == "admin" && values.email == "admin@gmail.com" && values.pass == 123456) {
//           localStorage.setItem("userAccessToken", uuidv4());
//           history.push("/home");
//         } else {
//           alert("Enter Valid Username, Email and Password");
//         }
//       };

//    return (
//         <>
//            <div>
//             <Container>
//                 <Card xs={6} className="mt-5 main-login-card">
//                     <div className="icon-style " >
//                       <SportsCricketTwoToneIcon className="icon-size"  ></SportsCricketTwoToneIcon>
//                     </div>
//                     <Formik
//                         enableReinitialize
//                          initialValues={{
//                             uname: '',
//                             email: '',
//                             pass: '',
//                         }}
//                         validationSchema={ValidationSchema}
//                         onSubmit={onHandleSubmit}
//                         >
//                         {({

//                           errors,
//                             touched,
//                             handleSubmit,

//                         }) => {
//                                             return (
//                                                 <>
//                                                 <Container>
//                                                     <form onSubmit={handleSubmit} className="mt-3">
//                                                         <Grid container spacing={3}>

//                                                          {/* First  Input */}
//                                                             <Grid item xs={12} sm={12}>
//                                                                 <Field
//                                                                     name="uname"
//                                                                     className="form-control"
//                                                                     placeholder="Enter username..."
//                                                                 />
//                                                             {errors.uname && touched.uname ? (
//                                                                             <div className="error">{errors.uname}</div>
//                                                                         ) : null}
//                                                             </Grid>
//                                                         {/* Second  Input */}
//                                                             <Grid item xs={12} sm={12}>
//                                                                 <Field
//                                                                     name="email"
//                                                                     className="form-control"
//                                                                     placeholder="Enter email..."
//                                                                 />
//                                                             {errors.email && touched.email ? (
//                                                                             <div className="error">{errors.email}</div>
//                                                                         ) : null}
//                                                             </Grid>

//                                                         {/* Third  Input */}
//                                                             <Grid item xs={12} sm={12}>
//                                                                 <Field
//                                                                     name="pass"
//                                                                     className="form-control"
//                                                                     placeholder="Enter password..."
//                                                                     type="password"
//                                                                 />
//                                                             {errors.pass && touched.pass ? (
//                                                                             <div className="error">{errors.pass}</div>
//                                                                         ) : null}
//                                                             </Grid>

//                                                         {/* Submit Button */}
//                                                             <Grid item xs={12} sm={12}>
//                                                                 <Button
//                                                                     variant="contained"
//                                                                     color="primary"
//                                                                     type="submit"
//                                                                     className="m-3"
//                                                                 >
//                                                                 Login
//                                                                 {/* <Link to="/" className="textAlign">Cancel</Link> */}
//                                                                 </Button>
//                                                             </Grid>
//                                                         </Grid>
//                                             </form>
//                                             </Container>
//                                             </>
//                                         );
//                                     }}
//                      </Formik>
//                 </Card>
//             </Container>
//             </div>
//         </>
//     )
// }

// export default Login
