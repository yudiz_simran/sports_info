/* eslint-disable react/prop-types */

import React, { Component } from 'react'
import { connect } from 'react-redux'

import { fetchspecificblogAction } from '../../Redux/Actions/getspecificAction'
import './dashboard.scss'

import { FaFacebookF } from 'react-icons/fa'
import { AiFillTwitterCircle } from 'react-icons/ai'
import { MdCall } from 'react-icons/md'
import { Markup } from 'interweave'
import { Card } from '@material-ui/core'
import VisibilityIcon from '@material-ui/icons/Visibility'
import CommentIcon from '@material-ui/icons/Comment'
import ScrollToTopBtn from './ScrollToTop'
class ViewDetail extends Component {
  componentDidMount () {
    this.getList()
  }

  //   Get Blog Id from url
  getID () {
    const { match } = this.props
    return match.params.id
  }

  getList () {
    const id = this.getID()
    // eslint-disable-next-line react/prop-types
    // eslint-disable-next-line no-undef
    // eslint-disable-next-line react/prop-types
    this.props.fetchspecificblogAction(id).then(() => {

    })
  }

  render () {
    const content = this.props.specificblogData.sDescription
    return (
    // eslint-disable-next-line react/jsx-no-comment-textnodes
    <>
    <div className="main-image mt-3">
      <img className="detail-view-image" src={this.props.specificblogData?.sImage} alt="Snow" style={{ width: '100%' }} />
        <div className="centered ">
            <h1 className="inner-h1-image">{this.props?.specificblogData?.sTitle}</h1>
            <div className="row col-lg-12">
                <h6 className="Uppercase col-lg-6">{this.props?.specificblogData?.iId?.sFirstName}{this.props?.specificblogData?.iId?.sLastName}</h6>
                <div className="col-lg-6 row ">
                      <div className="tag__item col-lg-3 icon-style" ><VisibilityIcon /> {this.props?.specificblogData?.nViewCounts}</div>
                     <div className="tag__item col-lg-3 icon-style" ><CommentIcon ></CommentIcon> {this.props?.specificblogData?.nCommentsCount} </div>
                </div>
             </div>
        </div>
    </div>
    <div className="main-content col-lg-12 col-md-12 row">
        <div className="col-lg-2 icons-div">
            <div className="icons">
                <div className="icon-1 icons">
                    <FaFacebookF></FaFacebookF>
                </div>
                <div className="icon-2 icons">
                    <AiFillTwitterCircle></AiFillTwitterCircle>
                </div>
                <div className="icon-3 icons">
                    <MdCall></MdCall>
                </div>
             </div>
        </div>
        <div className="col-lg-8  view-detail-div  mt-3">
         <Card className="main-content-card">
           <Markup content={content} className="con" />
         </Card>
          {/* <h4 className="center-text">{this.props.specificblogData?.sShortDesc}</h4> */}
                </div>
      </div>
             <ScrollToTopBtn />
            </>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    specifiblogLoading: state.SpecificblogReducer.isLoading,
    specificblogData: state.SpecificblogReducer.data?.data || [],
    specificblogError: state.SpecificblogReducer.error || {}
  }
}
const mapDispatchToProps = {
  fetchspecificblogAction
}

export default connect(mapStateToProps, mapDispatchToProps)(ViewDetail)
