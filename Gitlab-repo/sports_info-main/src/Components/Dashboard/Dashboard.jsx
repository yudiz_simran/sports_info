
import React, { Component } from 'react'

// NPM-Packages
import { connect } from 'react-redux'
import axios from 'axios'

// Action-files
import { fetchblogsAction } from '../../Redux/Actions/getBlogsActions'
import { fetchspecificblogAction } from '../../Redux/Actions/getspecificAction'

// Material-UI-Components
import { CircularProgress, Button } from '@material-ui/core'
import VisibilityIcon from '@material-ui/icons/Visibility'
import CommentIcon from '@material-ui/icons/Comment'

import './dashboard.scss'

// Components
import CircularSpinner from './Spinner'

import { Link } from 'react-router-dom'

class Dashboard extends Component {
  constructor (props) {
    super(props)
    this.state = {
      loading: false,
      nStart: 4,
      data: []
    }
    this.onClick = this.onClick.bind(this)
  }

  componentDidMount () {
    this.getList()
  }

  //  Get first four blogs
  async getList () {
    const response = await axios({
      method: 'post',
      url: 'https://backend.sports.info/api/v1/posts/recent',
      headers: { 'Content-Type': 'application/json' },
      data: {
        nStart: 0,
        nLimit: 4,
        eSort: 'Latest',
        bRemoveBannerPosts: true
      }
    })
    const final = await response.data.data
    this.setState(({ data: final, loading: false }))
  }

  //   To Load more blogs
  async onClick () {
    this.setState({ loading: true })
    this.setState(({ nStart: this.state.nStart + 4 }))

    // setTimeout(() => this.setState({ loading: false, }));
    const response = await axios({
      method: 'post',
      url: 'https://backend.sports.info/api/v1/posts/recent',
      headers: { 'Content-Type': 'application/json' },
      data: {
        nStart: this.state.nStart,
        nLimit: 4,
        eSort: 'Latest',
        bRemoveBannerPosts: true
      }
    })
    const final = await response.data.data
    this.setState({ loading: false })
    const array = this.state.data.concat(final)
    this.setState({ data: array })
  }

  //  To view specific blog
  viewSpecificBlog (id) {
    // eslint-disable-next-line react/prop-types
    this.props.history.push(`/detail/${id}`)
    // eslint-disable-next-line react/prop-types
    this.props.fetchspecificblogAction(id).then(() => {
    })
  }

  render () {
    return (
        <>
          <section className="light">
            <div className="container py-2">
                    {/* All blogs */}
                    {
                        // eslint-disable-next-line react/prop-types
                        (this.props.loading)
                          ? <center><CircularSpinner /></center>
                          : this.state.data.map((data) => {
                            return (
                             <>
                                <div className="card card-main container">
                                    <div className="card-body">
                                        <div className="col-lg-12 row">
                                                <div className="col-lg-3">
                                                <div className="postcard light blue">
                                                    <a className="postcard__img_link" href="#">
                                                        <img className="postcard__img img_border" src={data?.sImage} alt="Image Title" />
                                                    </a>
                                                </div>
                                                </div>
                                                <div className="col-lg-9">
                                                    <Link to={`/detail/${data?._id}`}><h5 className="main-title font-weight-bold" style={{ color: 'black' }}
                                                    >{data?.sTitle}</h5></Link>
                                                    <p className="description-text">{data?.sDescription}</p>
                                                    <div className="row col-lg-12">
                                                        <div className="row col-lg-6">
                                                            <h6 className="title-text">{data?.iId?.sFirstName}{data?.iId?.sLastName}</h6>
                                                        </div>
                                                        <div className="col-lg-6 row ">
                                                           <div className="tag__item col-lg-3 icon-style" style={{ color: 'gray' }}><VisibilityIcon /> {data?.nViewCounts}</div>
                                                           <div className="tag__item col-lg-3 icon-style" style={{ color: 'gray' }}><CommentIcon ></CommentIcon> {data?.nCommentsCount} </div>
                                                        </div>
                                                    </div>
                                                </div>
                                        </div>
                                        </div>
                                </div>
                             </>
                            )
                          })
                    }
                    <Button variant="contained" className="spinner-button mt-4" onClick={this.onClick} disabled={this.state.loading}>
                                                         {this.state.loading && <CircularProgress size={14} />}
                                                            {!this.state.loading && 'Load More'}
                                                    </Button>

                 </div>
              </section>
            </>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    blogsListLoading: state.GetBlogsReducer.isLoading,
    blogsListData: state.GetBlogsReducer.data?.data || [],
    blogsListError: state.GetBlogsReducer.error || {},

    specifiblogLoading: state.SpecificblogReducer.isLoading,
    specificblogData: state.SpecificblogReducer.data?.data || [],
    specificblogError: state.SpecificblogReducer.error || {}
  }
}

const mapDispatchToProps = {
  fetchblogsAction,
  fetchspecificblogAction
}

export default connect(mapStateToProps, mapDispatchToProps)(Dashboard)
