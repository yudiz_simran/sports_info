export const FETCH_SPECIFIC_BLOGS_INFO_REQUEST = 'FETCH_SPECIFIC_BLOGS_INFO_REQUEST'
export const FETCH_SPECIFIC_BLOGS_INFO_SUCCESS = 'FETCH_SPECIFIC_BLOGS_INFO_SUCCESS'
export const FETCH_SPECIFIC_BLOGS_INFO_FAILURE = 'FETCH_SPECIFIC_BLOGS_INFO_FAILURE'
