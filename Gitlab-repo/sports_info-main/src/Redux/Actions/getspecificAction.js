import {
  FETCH_SPECIFIC_BLOGS_INFO_REQUEST,
  FETCH_SPECIFIC_BLOGS_INFO_SUCCESS,
  FETCH_SPECIFIC_BLOGS_INFO_FAILURE
} from '../Types/Get_Specific_Blog'

import axios from 'axios'
export const fetchallblogsRequest = () => ({
  type: FETCH_SPECIFIC_BLOGS_INFO_REQUEST
})

export const fetchallblogsSuccess = (data) => ({
  type: FETCH_SPECIFIC_BLOGS_INFO_SUCCESS,
  data
})

export const fetchallblogsFailure = (error) => ({
  type: FETCH_SPECIFIC_BLOGS_INFO_FAILURE,
  error
})

export const fetchspecificblogAction = (id) => (dispatch) => {
  dispatch(fetchallblogsRequest())
  return axios
    .get(`https://backend.sports.info/api/v1/posts/view/${id}`)
    .then((data) => {
      dispatch(fetchallblogsSuccess(data))
    })
    .catch((error) => {
      dispatch(fetchallblogsFailure(error))
    })
}
