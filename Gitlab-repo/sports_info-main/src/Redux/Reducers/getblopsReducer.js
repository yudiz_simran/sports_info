import {
  FETCH_BLOGS_INFO_REQUEST,
  FETCH_BLOGS_INFO_SUCCESS,
  FETCH_BLOGS_INFO_FAILURE
} from '../Types/Get_Blogs/getblogs'

const initialState = {
  data: null,
  error: null,
  isLoading: false
}

const GetBlogsReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_BLOGS_INFO_REQUEST:
      return {
        ...state,
        isLoading: true
      }
    case FETCH_BLOGS_INFO_SUCCESS:
      return {
        ...state,
        isLoading: false,
        data: action.data.data,
        error: null
      }
    case FETCH_BLOGS_INFO_FAILURE:
      return {
        ...state,
        isLoading: false,
        data: null,
        error: action.error.response
      }
    default:
      return state
  }
}

export default GetBlogsReducer
