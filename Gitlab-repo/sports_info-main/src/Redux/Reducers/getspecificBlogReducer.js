import {
  FETCH_SPECIFIC_BLOGS_INFO_REQUEST,
  FETCH_SPECIFIC_BLOGS_INFO_SUCCESS,
  FETCH_SPECIFIC_BLOGS_INFO_FAILURE
} from '../Types/Get_Specific_Blog'

const initialState = {
  data: null,
  error: null,
  isLoading: false
}

const SpecificblogReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_SPECIFIC_BLOGS_INFO_REQUEST:
      return {
        ...state,
        isLoading: true
      }
    case FETCH_SPECIFIC_BLOGS_INFO_SUCCESS:
      return {
        ...state,
        isLoading: false,
        data: action.data.data,
        error: null
      }
    case FETCH_SPECIFIC_BLOGS_INFO_FAILURE:
      return {
        ...state,
        isLoading: false,
        data: null,
        error: action.error.response
      }
    default:
      return state
  }
}

export default SpecificblogReducer
