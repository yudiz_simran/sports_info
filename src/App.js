/* eslint-disable react/jsx-no-comment-textnodes */
/* eslint-disable react/react-in-jsx-scope */
/* eslint-disable react/prop-types */

import './App.css';
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";

import { Provider } from "react-redux";
import { createStore, applyMiddleware } from "redux";
import reduxThunk from "redux-thunk";
import logger from "redux-logger";
import reducers from "./Redux/Reducers"



/**********************Bootstrap-styles-library*****************/
import 'bootstrap/dist/css/bootstrap.min.css';


import Dashboard from './Components/Dashboard/Dashboard';
import Sidebar from './Components/Sidebar/Sidebar';
// import Footer from './Components/Footer/Footer'
import ViewDetail from './Components/Dashboard/ViewDetail';
// import { Dashboard } from '@material-ui/icons';


const createStoreWithMiddleware =
  // eslint-disable-next-line no-undef
  process.env.NODE_ENV === "development"
    ? applyMiddleware(logger, reduxThunk)(createStore)
    : applyMiddleware(reduxThunk)(createStore);

const store = createStoreWithMiddleware(reducers, window.devToolsExtension ? window.devToolsExtension() : f => f);




function App() {
  return (
    // eslint-disable-next-line react/react-in-jsx-scope
    // eslint-disable-next-line react/jsx-no-comment-textnodes
    <Provider store={store}>
  
    <div  className="bg-image">
     <Sidebar></Sidebar>
      <Router>
        <Switch>
         <Route
          exact
          name="detail"
          path="/detail/:id"
          component={ViewDetail}
          />
          <Route
          exact
          name="Home"
          path="/home"
          component={Dashboard}
          />
          <Route
          exact
          name="Home"
          path="/"
          component={Dashboard}
          />
        </Switch>
      </Router>
      {/* <Footer></Footer> */}
    </div>
    </Provider>
  );
}

export default App;
