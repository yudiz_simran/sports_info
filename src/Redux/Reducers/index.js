import { combineReducers } from 'redux'

import GetBlogsReducer from './getblopsReducer'
import SpecificblogReducer from './getspecificBlogReducer'

const rootReducer = combineReducers({
  GetBlogsReducer: GetBlogsReducer,
  SpecificblogReducer: SpecificblogReducer
})

export default rootReducer
