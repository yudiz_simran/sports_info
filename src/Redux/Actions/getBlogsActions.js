import {
  FETCH_BLOGS_INFO_REQUEST,
  FETCH_BLOGS_INFO_SUCCESS,
  FETCH_BLOGS_INFO_FAILURE
} from '../Types/Get_Blogs/getblogs'

import axios from 'axios'
export const fetchallblogsRequest = () => ({
  type: FETCH_BLOGS_INFO_REQUEST
})
export const fetchallblogsSuccess = (data) => ({
  type: FETCH_BLOGS_INFO_SUCCESS,
  data
})

export const fetchallblogsFailure = (error) => ({
  type: FETCH_BLOGS_INFO_FAILURE,
  error
})

export const fetchblogsAction = () => (dispatch) => {
  dispatch(fetchallblogsRequest())
  return axios
    .post('https://backend.sports.info/api/v1/posts/recent')
    .then((data) => {
      dispatch(fetchallblogsSuccess(data))
    })
    .catch((error) => {
      dispatch(fetchallblogsFailure(error))
    })
}
