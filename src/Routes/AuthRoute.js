import React from "react";
import { Redirect, Route, Switch } from "react-router-dom";

// components
import Home from '../Components/Home/home'

const AuthRoute = () => {
  return (
    <Switch>
      <Route
        exact
        name="Home"
        path="/home"
        component={Home}
      />

      <Route exact name="Home" path="/home" component={Home} />
      <Redirect from="/" to="/login" />
    </Switch>
  )
}
export default React.memo(AuthRoute)
