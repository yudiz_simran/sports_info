/* eslint-disable react/prop-types */

import React, { Component } from 'react'
import { connect } from 'react-redux'

import { fetchspecificblogAction } from '../../Redux/Actions/getspecificAction'
import './dashboard.scss'

import { FaFacebookF } from 'react-icons/fa'
import { AiFillTwitterCircle } from 'react-icons/ai'
import { MdCall } from 'react-icons/md'
import { Markup } from 'interweave'

class ViewDetail extends Component {
  componentDidMount () {
    this.getList()
  }

  //   Get Blog Id from url
  getID () {
    const { match } = this.props
    return match.params.id
  }

  getList () {
    const id = this.getID()
    // eslint-disable-next-line react/prop-types
    // eslint-disable-next-line no-undef
    // eslint-disable-next-line react/prop-types
    this.props.fetchspecificblogAction(id).then(() => {

    })
  }

  render () {
    const content = this.props.specificblogData.sDescription
    return (
    // eslint-disable-next-line react/jsx-no-comment-textnodes
    <>
    <div className="main-image mt-3">
      <img className="detail-view-image" src={this.props.specificblogData?.sImage} alt="Snow" style={{ width: '100%' }} />
        <div className="centered">
            <h1>{this.props?.specificblogData?.sTitle}</h1>
        </div>
    </div>
    <div className="main-content col-lg-12 col-md-12 row">
        <div className="col-lg-2">
            <div className="icons">
                <div className="icon-1 icons">
                    <FaFacebookF></FaFacebookF>
                </div>
                <div className="icon-2 icons">
                    <AiFillTwitterCircle></AiFillTwitterCircle>
                </div>
                <div className="icon-3 icons">
                    <MdCall></MdCall>
                </div>
             </div>
        </div>
    <div className="col-lg-10">
        <Markup content={content} className="con" />
          <h4 className="center-text">{this.props.specificblogData?.sShortDesc}</h4>
                </div>
             </div>
            </>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    specifiblogLoading: state.SpecificblogReducer.isLoading,
    specificblogData: state.SpecificblogReducer.data?.data || [],
    specificblogError: state.SpecificblogReducer.error || {}
  }
}
const mapDispatchToProps = {
  fetchspecificblogAction
}

export default connect(mapStateToProps, mapDispatchToProps)(ViewDetail)
